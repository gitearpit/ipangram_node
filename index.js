import dotenv from "dotenv";
dotenv.config();
import express from "express";
import routes from "./routes/index.js";
import dbConfig from "./models/db_config.js";
import bodyParser from "body-parser";
import cors from "cors";

const app = express();

const PORT = process.env.PORT || 5000;
const DB_URL = process.env.DB_URL;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

dbConfig(DB_URL);
routes(app);

app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
});