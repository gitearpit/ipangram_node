import departmentsRoute from "./departments.js";
import userRoute from "./users.js";

export default function routes(app) {
    app.use(`/users`, userRoute);
    app.use(`/department`, departmentsRoute);
}