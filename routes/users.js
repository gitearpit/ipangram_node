import express from "express";
import userController from "../controllers/users/index.js";
import authenticateAndAuthorize from "../utils/middleware/user_auth.js";
const userRoute = express.Router();

userRoute.post(`/login`, userController.login)
userRoute.post(`/register`, userController.createUser)
userRoute.delete(`/delete/:id?`, authenticateAndAuthorize, userController.delete)
userRoute.get(`/find/:id?`, userController.getUserById)
userRoute.put(`/update/:id?`, authenticateAndAuthorize, userController.updateUserById)
userRoute.get(`/get_unassigned`, authenticateAndAuthorize, userController.get_unassigned)
userRoute.post(`/get`, authenticateAndAuthorize, userController.getAllUsers)

export default userRoute;