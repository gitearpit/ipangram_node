import express from "express";
import authenticateAndAuthorize from "../utils/middleware/user_auth.js";
import departmentController from "../controllers/departments/index.js";
const departmentsRoute = express.Router();

departmentsRoute.delete(`/delete/:id?`, authenticateAndAuthorize, departmentController.delete)
departmentsRoute.post(`/create`, authenticateAndAuthorize, departmentController.createDepartment)
departmentsRoute.get(`/find/:id?`, authenticateAndAuthorize, departmentController.getDepartmentById)
departmentsRoute.put(`/update/:id?`, authenticateAndAuthorize, departmentController.updateDepartmentById)
departmentsRoute.get(`/get`, authenticateAndAuthorize, departmentController.getAllDepartment)
departmentsRoute.get(`/get-master`, authenticateAndAuthorize, departmentController.getDepartmentMaster)

export default departmentsRoute;