import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import UserModel from "../../models/users.js";

const userController = {

  getAllUsers: async (req, res) => {
    try {
      const { dataFor } = req.body;
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      else if (dataFor) {
        let respData;
        if (dataFor === "IT") {
          respData = await UserModel.find({
            "category.label": "IT",
            "location.label": /^A/i,
            "role": "employee"
          }, { password: 0, __v: 0 });
        } else {
          respData = await UserModel.find({
            "category.label": "Sales",
            "role": "employee"
          }, { password: 0, __v: 0 })
            .sort({ "lastName": -1, "firstName": -1 });
        }

        return res.status(200).json({ message: 'users', data: respData, success: true });
      }
      const data = await UserModel.find({ role: "employee" }, { password: 0, __v: 0 });
      res.status(200).json({ message: 'users', data, success: true });
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  get_unassigned: async (req, res) => {
    try {
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const data = await UserModel.find({ role: "employee", department: null }, { password: 0, __v: 0 });
      res.status(200).json({ message: 'users', data, success: true });
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  getUserById: async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) {
        return res.status(401).json({ message: "Invalid request", success: false });
      }
      const user = await UserModel.findById(id, { __v: 0, password: 0 });
      if (user) {
        res.status(200).json({ message: "User data", data: user, success: true });
      } else {
        res.status(404).json({ message: "User not found", data: null, success: false });
      }
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  createUser: async (req, res) => {
    try {
      const { email } = req.body;
      const exist = await UserModel.findOne({ email });
      if (exist) {
        return res.status(409).json({ message: "User already exists with this email.", success: false });
      }
      const hashedPassword = await bcrypt.hash(req.body.password, 10);
      req.body.password = hashedPassword;
      const newData = await UserModel.create(req.body);
      res.status(200).json({ message: "User registered.", data: newData, success: true });
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  updateUserById: async (req, res) => {
    try {
      const { id } = req.params;
      const data = req.body;
      if (!id) {
        return res.status(401).json({ message: "Invalid request", success: false });
      }
      else if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const updatedUser = await UserModel.findByIdAndUpdate(id, data, {
        new: true,
        select: "-__v -password"
      });
      if (updatedUser) {
        return res.status(200).json({ message: "User data updated", data: updatedUser, success: true });
      } else {
        return res.status(404).json({ message: "User not found", data: null, success: false });
      }
    } catch (error) {
      return res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  login: async (req, res) => {
    const SECRET_KEY = process.env.SECRET_KEY;
    try {
      const { email, password } = req.body;
      if (!email || !password) {
        return res.status(400).json({ message: "Email and password are required", success: false });
      }
      const user = await UserModel.findOne({ email });
      if (!user) {
        return res.status(404).json({ message: "User not found", success: false });
      }
      const isPasswordValid = await bcrypt.compare(password, user.password);
      if (!isPasswordValid) {
        return res.status(401).json({ message: "Invalid password", success: false });
      }
      const token = jwt.sign({ user }, SECRET_KEY);
      return res.status(200).json({ message: "Login successful", token, user, success: true });
    } catch (error) {
      return res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  delete: async (req, res) => {
    const userId = req.params.id;
    try {
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const deletedUser = await UserModel.findByIdAndDelete(userId);
      if (!deletedUser) {
        return res.status(404).json({ error: 'User not found' });
      }
      return res.status(200).json({ message: 'User deleted successfully', success: true });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Internal server error' });
    }
  }

}

export default userController;