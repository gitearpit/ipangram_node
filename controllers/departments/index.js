import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import DepartmentModel from "../../models/department.js";
import UserModel from "../../models/users.js";
import DepartmentMasterModel from "../../models/department_master.js";

const departmentController = {

  getAllDepartment: async (req, res) => {
    try {
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const data = await DepartmentModel.find({}, { __v: 0 });
      res.status(200).json({ message: 'departments', data, success: true });
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  getDepartmentById: async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) {
        return res.status(401).json({ message: "Invalid request", success: false });
      }
      else if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const departments = await DepartmentModel.findById(id, { __v: 0, password: 0 });
      if (departments) {
        res.status(200).json({ message: "Departments data", data: departments, success: true });
      } else {
        res.status(404).json({ message: "Departments not found", data: null, success: false });
      }
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  createDepartment: async (req, res) => {
    try {
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const { name, category, location } = req.body;
      const exist = await DepartmentModel.findOne({ name, category, location });
      if (exist) {
        return res.status(409).json({ message: "Department already exists with this details.", success: false });
      }
      const newData = await DepartmentModel.create(req.body);
      res.status(200).json({ message: "Department registered.", data: newData, success: true });
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  updateDepartmentById: async (req, res) => {
    try {
      const { id } = req.params;
      const data = req.body;
      if (!id) {
        return res.status(401).json({ message: "Invalid request", success: false });
      }
      else if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const updatedDepartment = await DepartmentModel.findByIdAndUpdate(id, data);
      if (!updatedDepartment) {
        return res.status(404).json({ message: "Department not found", data: null, success: false });
      }
      const oldEmployees = updatedDepartment.employees.map(e => e.value);
      const newEmployees = data.employees.map(e => e.value);
      await UserModel.updateMany(
        { _id: { $in: oldEmployees } },
        { $set: { salary: null, category: null, location: null, department: null } }
      );
      await UserModel.updateMany(
        { _id: { $in: newEmployees } },
        { $set: { salary: data.salary, category: data.category, location: data.location, department: data.name } }
      );
      return res.status(200).json({ message: "Department data updated", data: updatedDepartment, success: true });
    } catch (error) {
      return res.status(500).json({ message: "Internal server error", error, success: false });
    }
  },

  delete: async (req, res) => {
    const departmentId = req.params.id;
    try {
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const deletedDepartmentData = await DepartmentModel.find({ _id: departmentId });
      const departmentEmployees = deletedDepartmentData[0].employees.map(e => e.value);

      const deletedDepartment = await DepartmentModel.findByIdAndDelete(departmentId);
      if (!deletedDepartment) {
        return res.status(404).json({ error: 'Department not found' });
      }
      await UserModel.updateMany(
        { _id: { $in: departmentEmployees } },
        { $set: { salary: null, category: null, location: null, department: null } }
      );
      return res.status(200).json({ message: 'Department deleted successfully', success: true });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Internal server error' });
    }
  },

  getDepartmentMaster: async (req, res) => {
    try {
      if (req.user.role !== "manager") {
        return res.status(401).json({ message: "Require manager access...", success: false });
      }
      const data = await DepartmentMasterModel.find({}, { __v: 0 });
      res.status(200).json({ message: 'departments_master', data, success: true });
    } catch (error) {
      res.status(500).json({ message: "Internal server error", error, success: false });
    }
  }

}

export default departmentController;