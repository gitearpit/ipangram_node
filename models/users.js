import mongoose from 'mongoose';
const { Schema } = mongoose;

const userSchema = new Schema(
    {
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        gender: { type: String, default: "male" },
        hobbies: String,
        department: { type: Object, default: null },
        category: Object,
        location: Object,
        salary: Number,
        employeeId: String,
        role: { type: String, default: "employee" },
    },
    { timestamps: true }
);

userSchema.pre('save', async function (next) {
    if (!this.employeeId) {
        const serialNumber = await this.constructor.countDocuments().exec();
        this.employeeId = `i-${this.firstName.toLowerCase()}-${serialNumber + 1}`;
    }
    next();
});

const UserModel = mongoose.model('user', userSchema);

export default UserModel;