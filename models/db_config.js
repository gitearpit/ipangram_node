import mongoose from "mongoose";

const dbConfig =  (DB_URL) => {
     mongoose.connect(DB_URL)
        .then(res => {
            console.log('Connected to DB');
        })
        .catch(err => {
            console.log("error =>", err);
        })
}

export default dbConfig;