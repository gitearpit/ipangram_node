import mongoose from 'mongoose';
const { Schema } = mongoose;

const departmentMasterSchema = new Schema(
    {
        department: Object,
        category: Array,
        location: Array,
    },
    { timestamps: true }
);

const DepartmentMasterModel = mongoose.model('master', departmentMasterSchema);

export default DepartmentMasterModel;