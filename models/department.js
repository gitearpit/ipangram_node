import mongoose from 'mongoose';
const { Schema } = mongoose;

const departmentSchema = new Schema(
    {
        name: Object,
        category: Object,
        employees: { type: Array, default: [] },
        location: Object,
        salary: Number,
    },
    { timestamps: true }
);

const DepartmentModel = mongoose.model('department', departmentSchema);

export default DepartmentModel;