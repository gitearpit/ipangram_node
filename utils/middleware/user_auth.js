import jwt from 'jsonwebtoken';

const authenticateAndAuthorize = (req, res, next) => {
  const SECRET_KEY = process.env.SECRET_KEY;
  const token = req.header('Authorization');
  if (!token) {
    return res.status(401).json({ message: 'No token found. Authorization denied.' });
  }
  try {
    const tokenParts = token.split(" ");
    const extractedToken = tokenParts[1];
    const decoded = jwt.verify(extractedToken, SECRET_KEY);
    req.user = decoded.user;
    next();
  } catch (err) {
    res.status(401).json({ message: 'Invalid token. Authorization denied.' });
  }
};

export default authenticateAndAuthorize;
